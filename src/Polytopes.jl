module Polytopes

export ineqLusztig, latticePointsLusztig, numLatticePointsLusztig

using Oscar

PM = Oscar.Polymake.polytope

ineqA(n::Int64, l::Int64, p::Matrix{Int64}) = [
    l,
    (p[i, j] for j in 2:n+1 for i in 1:j-1)...
] :: Vector{Int64}

ineqBC(n::Int64, l::Int64, p::Matrix{Int64}) = [
    l,
    (p[i, j] for j in 2:n for i in 1:j-1)...,
    (p[i+n, j] for j in 2:n+1 for i in 1:j-1)...
] :: Vector{Int64}

ineqD(n::Int64, l::Int64, p::Matrix{Int64}) = [
    l,
    (p[i, j] for j in 2:n for i in 1:j-1)...,
    (p[i+n, j] for j in 2:n for i in 1:j-1)...  
] :: Vector{Int64}


function ineqLusztigA(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= 1 21 321 ... n...1`` or ``ω_0= n (n-1)n (n-2)(n-1)n... 1...n``
    res = Vector{Vector{Int64}}()
    
    for i in 1:n, j in i+1:n+1
        p = zeros(Int64, n+1, n+1)

        p[i, j:n+1]     .+= -1
        p[i+1, j+1:n+1] .+=  1

        push!(res, ineqA(n, lambda[i], p))
    end

    for i in 1:n, j in i+1:n+1
        p = zeros(Int64, n+1, n+1)
        p[i, j] = 1
        push!(res, ineqA(n, 0, p))
    end

    return res
end

function ineqLusztigB(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= 1 21 321 ... (n-1)...1 n (n-1)n (n-2)(n-1)n... 1...n``
    res = Vector{Vector{Int64}}()

    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n+1, n+2)

        p[i, j:n]            .+= -1
        p[i+n, i+1:n+1]      .+= -1
        p[(1:i-1) .+ n, i+1] .+= -1
        p[i+1, j+1:n]        .+=  1
        p[i+1+n, i+2:n+1]    .+=  1
        p[(1:i) .+ n, i+2]   .+=  1

        push!(res, ineqBC(n, lambda[i], p))
    end

    for i in 1:n-1, j in i+2:n+1
        p = zeros(Int64, 2n+1, n+2)

        p[i+n, j:n+1]     .+= -1
        p[i+1+n, j+1:n+1] .+=  1

        push!(res, ineqBC(n, lambda[i], p))
    end

    p = zeros(Int64, 2n+1, n+1)
    p[n+n, n+1] = -1
    push!(res, ineqBC(n, lambda[n], p))
    
    for j in 2:n, i in 1:j-1
        p = zeros(Int64, 2n+1, n+1)

        p[(i:j-1) .+ n, j]   .+= -1
        p[(i+1:j) .+ n, j+1] .+=  1
        p[j-1+n, j+1:n+1]    .+= -1
        p[j+n, j+2:n+1]      .+=  1
        
        push!(res, ineqBC(n, lambda[j-1], p))
    end
    
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n+1)
        p[i, j] = 1
        push!(res, ineqBC(n, 0, p))
    end

    for i in 1:n, j in i+1:n+1
        p = zeros(Int64, 2n, n+1)
        p[i+n, j] = 1
        push!(res, ineqBC(n, 0, p))
    end

    return res
end


function ineqLusztigBv2(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= (n-1) (n-2)(n-1) (n-3)(n-2)(n-1) ... 1...(n-1) n (n-1)n (n-2)(n-1)n... 1...n``
    res = Vector{Vector{Int64}}()

    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n+1, n+2)

        p[i, j:n]                .+= -1
        p[n-i+n, n-i+2:n+1]      .+= -1
        p[(1:n-i) .+ n, n-i+1]   .+= -1
        p[i+1, j+1:n]            .+=  1
        p[2n-i+1, n-i+3:n+1]     .+=  1
        p[(1:n-i+1) .+ n, n-i+2] .+=  1
        
        push!(res, ineqBC(n, lambda[n-i], p))
    end
    
    for i in 1:n-1, j in i+2:n+1
        p = zeros(Int64, 2n+1, n+2)

        p[i+n, j:n+1]     .+= -1
        p[i+1+n, j+1:n+1] .+=  1
    
        push!(res, ineqBC(n, lambda[i], p))
    end

    p = zeros(Int64, 2n+1, n+1)
    p[n+n, n+1] = -1
    push!(res, ineqBC(n, lambda[n], p))
    
    for j in 2:n, i in 1:j-1
        p = zeros(Int64, 2n+1, n+1)

        p[(i:j-1) .+ n, j]   .+= -1
        p[(i+1:j) .+ n, j+1] .+=  1
        p[j-1+n, j+1:n+1]    .+= -1
        p[j+n, j+2:n+1]      .+=  1

        push!(res, ineqBC(n, lambda[j-1], p))
    end
    
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n+1)
        p[i, j] = 1
        push!(res, ineqBC(n, 0, p))
    end

    for i in 1:n, j in i+1:n+1
        p = zeros(Int64, 2n, n+1)
        p[i+n, j] = 1
        push!(res, ineqBC(n, 0, p))
    end

    return res
end


function ineqLusztigC(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= 1 21 321 ... (n-1)...1 n (n-1)n (n-2)(n-1)n... 1...n``
    res = Vector{Vector{Int64}}()
     
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n+1, n+2)

        p[i, j:n]            .+= -1
        p[i+n, i+1:n+1]      .+= -1
        p[(1:i) .+ n, i+1]   .+= -1
        p[i+1, j+1:n]        .+=  1
        p[i+1+n, i+2:n+1]    .+=  1
        p[(1:i+1) .+ n, i+2] .+=  1

        push!(res, ineqBC(n, lambda[i], p))
    end
        
    for i in 1:n-1, j in i+2:n+1
        p = zeros(Int64, 2n+1, n+2)
        
        p[i+n, j:n+1]     .+= -1
        p[i+1+n, j+1:n+1] .+=  1
        
        push!(res, ineqBC(n, lambda[i], p))
    end

    p = zeros(Int64, 2n+1, n+1)
    p[n+n, n+1] = -1
    push!(res, ineqBC(n, lambda[n], p))
    
    for j in 2:n, i in 1:j-1
        p = zeros(Int64, 2n+1, n+1)
        p[(i:j-1) .+ n, j]   .+= -1
        p[(i+1:j) .+ n, j+1] .+=  1
        p[j-1+n, j:n+1]      .+= -1
        p[j+n, j+1:n+1]      .+=  1
        push!(res, ineqBC(n, lambda[j-1], p))
    end
    
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n+1)
        p[i, j] = 1
        push!(res, ineqBC(n, 0, p))
    end

    for i in 1:n, j in i+1:n+1
        p = zeros(Int64, 2n, n+1)
        p[i+n, j] = 1
        push!(res, ineqBC(n, 0, p))
    end
    
    return res
end


function ineqLusztigCv2(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= (n-1) (n-2)(n-1) (n-3)(n-2)(n-1) ... 1...(n-1) n (n-1)n (n-2)(n-1)n... 1...n``
    res = Vector{Vector{Int64}}()

    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n+1, n+2)

        p[i, j:n]                .+= -1
        p[2n-i, n-i+1:n+1]       .+= -1
        p[(1:n-i) .+ n, n-i+1]   .+= -1
        p[i+1, j+1:n]            .+=  1
        p[2n-i+1, n-i+2:n+1]     .+=  1
        p[(1:n-i+1) .+ n, n-i+2] .+=  1

        push!(res, ineqBC(n, lambda[n-i], p))
    end
        
    for i in 1:n-1, j in i+2:n+1
        p = zeros(Int64, 2n+1, n+2)

        p[i+n, j:n+1]     .+= -1
        p[i+1+n, j+1:n+1] .+=  1

        push!(res, ineqBC(n, lambda[i], p))
    end

    p = zeros(Int64, 2n+1, n+1)
    p[n+n, n+1] = -1
    push!(res, ineqBC(n, lambda[n], p))
    
    for j in 2:n, i in 1:j-1
        p = zeros(Int64, 2n+1, n+1)

        p[(i:j-1) .+ n, j]   .+= -1
        p[(i+1:j) .+ n, j+1] .+=  1
        p[j-1+n, j:n+1]      .+= -1
        p[j+n, j+1:n+1]      .+=  1

        push!(res, ineqBC(n, lambda[j-1], p))
    end
    
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n+1)
        p[i, j] = 1
        push!(res, ineqBC(n, 0, p))
    end

    for i in 1:n, j in i+1:n+1
        p = zeros(Int64, 2n, n+1)
        p[i+n, j] = 1
        push!(res, ineqBC(n, 0, p))
    end
    
    return res
end


function ineqLusztigD(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= 1 21 321 ... (n-1)...1 n (n-2)(n-1) (n-3)(n-2)n (n-4)(n-3)(n-2)(n-1)... 1...(n-2)(n-1)/n``
    res = Vector{Vector{Int64}}()

    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n+1, n+1)

        p[i, j:n]          .+= -1
        p[i+n, i+1:n]      .+= -1
        p[(1:i-1) .+ n, i] .+= -1
        p[i+1, j+1:n]      .+=  1
        p[i+1+n, i+2:n]    .+=  1
        p[(1:i) .+ n, i+1] .+=  1
        
        push!(res, ineqD(n, lambda[i], p))
    end
        
    for i in 1:n-2, j in i+2:n
        p = zeros(Int64, 2n+1, n+1)

        p[i+n, j:n]     .+= -1
        p[i+1+n, j+1:n] .+= 1

        push!(res, ineqD(n, lambda[i], p))
    end

    p = zeros(Int64, 2n+1, n+1)
    p[n-1+n, n] = -1
    push!(res, ineqD(n, lambda[n], p))
    
    for j in 2:n-1, i in 1:j-1
        p = zeros(Int64, 2n+1, n+1)

        p[(i:j-1) .+ n, j]   .+= -1
        p[(i+1:j) .+ n, j+1] .+=  1
        p[j+n, j+1:n]        .+= -1
        p[j+1+n, j+2:n]      .+=  1

        push!(res, ineqD(n, lambda[j], p))
    end
    
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n)
        p[i, j] = 1
        push!(res, ineqD(n, 0, p))
    end

    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n)
        p[i+n, j] = 1
        push!(res, ineqD(n, 0, p))
    end

    return res
end


function ineqLusztigDv2(n::Int64, lambda::Vector{Int64}) :: Vector{Vector{Int64}}
    # reduced decomposition for ``ω_0= (n-1) (n-2)(n-1) (n-3)(n-2)(n-1) ... 1...(n-1) n (n-2)(n-1) (n-3)(n-2)n (n-4)(n-3)(n-2)(n-1)... 1...(n-2)(n-1)/n``
    res = Vector{Vector{Int64}}()

    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n+1, n+1)

        p[i, j:n]               .+= -1
        p[2n-i, n-i+1:n]        .+= -1
        p[(1:n-i-1) .+ n, n-i]  .+= -1
        p[i+1, j+1:n]           .+=  1
        p[2n-i+1, n-i+2:n]      .+=  1
        p[(1:n-i) .+ n, n-i+1]  .+=  1

        push!(res, ineqD(n, lambda[n-i], p))
    end

    for i in 1:n-2, j in i+2:n
        p = zeros(Int64, 2n+1, n+1)

        p[i+n, j:n]     .+= -1
        p[i+1+n, j+1:n] .+= 1

        push!(res, ineqD(n, lambda[i], p))
    end

    p = zeros(Int64, 2n+1, n+1)
    p[n-1+n, n] = -1
    push!(res, ineqD(n, lambda[n], p))
    
    for j in 2:n-1, i in 1:j-1
        p = zeros(Int64, 2n+1, n+1)

        p[(i:j-1) .+ n, j]   .+= -1
        p[(i+1:j) .+ n, j+1] .+= 1
        p[j+n, j+1:n]        .+= -1
        p[j+1+n, j+2:n]      .+= 1

        push!(res, ineqD(n, lambda[j], p))
    end
    
    p = zeros(Int64, 2n, n)
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n)
        p[i, j] = 1
        push!(res, ineqD(n, 0, p))
    end
        
    for i in 1:n-1, j in i+1:n
        p = zeros(Int64, 2n, n)
        p[i+n, j] = 1
        push!(res, ineqD(n, 0, p))
    end
    
    return res
end

"""
    ineqLusztig(dynkin::Char, n::Int64, lambda::Vector{Int64}; variant=1::Int64) :: Vector{Vector{Int64}}

Computes the inequalities to define the Lusztig polytope of the simple Lie algebra specified by `dynkin` and `n` with hightest weight ``ω = \\sum_{i=1}^{n} λ_i ω_i``.  It is used by [`latticePointsLusztig`](@ref). For each type, there are computations for two reduced decompositions for the longest word ``ω_0`` in the Weyl group available. The choice can be made via `variant`. As in type `A` the inequalities are the same for both words, the variable `variant` is ignored in this case.

For dynkin type `A` the inequalities are calculated for `ω_0= 1 21 321 ... n...1` or `n (n-1)n (n-2)(n-1)n ... 1...n`.\\
For dynkin types `B` and `C` the variant `1` uses `ω_0= 1 21 321 ... (n-1)...1 n (n-1)n (n-2)(n-1)n ... 1...n`.\\
For dynkin types `B` and `C` the variant `2` uses `ω_0= (n-1) (n-2)(n-1) (n-3)(n-2)(n-1) ... 1...(n-1) n (n-1)n (n-2)(n-1)n ... 1...n`.\\
For dynkin type `D` the variant `1` uses `ω_0= 1 21 321 ... (n-1)...1 n (n-2)(n-1) (n-3)(n-2)n (n-4)(n-3)(n-2)(n-1) ... 1...(n-2)(n-1)/n`.\\
For dynkin type `D` the variant `2` uses `ω_0= (n-1) (n-2)(n-1) (n-3)(n-2)(n-1) ... 1...(n-1) n (n-2)(n-1) (n-3)(n-2)n (n-4)(n-3)(n-2)(n-1) ... 1...(n-2)(n-1)/n`.
"""
function ineqLusztig(dynkin::Char, n::Int64, lambda::Vector{Int64}; variant=1::Int64) :: Vector{Vector{Int64}}
    @assert (n > 0)
    @assert (n == length(lambda))

    if dynkin == 'A'
        res = ineqLusztigA(n, lambda)
    elseif dynkin == 'B'
        if variant == 1
            res = ineqLusztigB(n, lambda)
        elseif variant == 2
            res = ineqLusztigBv2(n, lambda)
        else
            error("Variant not supported")
        end
    elseif dynkin == 'C'
        if variant == 1
            res = ineqLusztigC(n, lambda)
        elseif variant == 2
            res = ineqLusztigCv2(n, lambda)
        else
            error("Variant not supported")
        end
    elseif dynkin == 'D'
        if variant == 1
            res = ineqLusztigD(n, lambda)
        elseif variant == 2
            res = ineqLusztigDv2(n, lambda)
        else
            error("Variant not supported")
        end
    else
        error("Dynkin type not supported")
    end

    return res
end

"""
    latticePointsLusztig(dynkin::Char, n::Int64, lambda::Vector{Int64}; variant=1::Int64) :: Matrix{Int64}

Computes the latticepoints in the Lusztig polytope of the simple Lie algebra specified by `dynkin` and `n` with hightest weight ``ω = \\sum_{i=1}^{n} λ_i ω_i``.

The `variant` parameter influences the used reduced composition of the longest weyl group element ``ω_0`` as above.
"""
function latticePointsLusztig(dynkin::Char, n::Int64, lambda::Vector{Int64}; variant=1::Int64) :: Matrix{Int64}
    @assert (n > 0)
    @assert (n == length(lambda))

    ineqs = ineqLusztig(dynkin, n, lambda; variant=variant)

    vecvecToMatrix(vv) = Matrix{Int64}(transpose(reshape(reduce(vcat,vv), :, length(vv))))

    p = PM.Polytope(INEQUALITIES=vecvecToMatrix(ineqs))
    lp = p.LATTICE_POINTS_GENERATORS[1]

    return Matrix{Int64}(lp)
end


"""
    numLatticePointsLusztig(dynkin::Char, n::Int64, lambda::Vector{Int64}; variant=1::Int64) :: Int64

Returns the dimension of the hightest-weight module over the simple Lie algebra specified by `dynkin` and `n` with hightest weight ``ω = \\sum_{i=1}^{n} λ_i ω_i``. The algorithm counts the latticepoints computed by [`latticePointsLusztig`](@ref).

Examples
======
Dynkin Type A
------
adjoint representation of ``\\mathfrak{sl}_4``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('A', 3, [1,0,1])
15
```
natural representation of ``\\mathfrak{sl}_5``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('A', 4, [1,0,0,0])
5
```

Dynkin Type B
------
adjoint representation of ``\\mathfrak{o}_7``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('B', 3, [0,1,0])
21

julia> PolyBases.Polytopes.numLatticePointsLusztig('B', 3, [0,1,0]; variant=2)
21
```
natural representation of ``\\mathfrak{o}_9``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('B', 4, [1,0,0,0])
9

julia> PolyBases.Polytopes.numLatticePointsLusztig('B', 4, [1,0,0,0]; variant=2)
9
```

Dynkin Type C
------
adjoint representation of ``\\mathfrak{sp}_6``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('C', 3, [2,0,0])
21

julia> PolyBases.Polytopes.numLatticePointsLusztig('C', 3, [2,0,0]; variant=2)
21
```
natural representation of ``\\mathfrak{sp}_8``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('C', 4, [1,0,0,0])
8

julia> PolyBases.Polytopes.numLatticePointsLusztig('C', 4, [1,0,0,0]; variant=2)
8
```

Dynkin Type D
------
adjoint representation of ``\\mathfrak{o}_6``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('D', 3, [0,1,1])
15

julia> PolyBases.Polytopes.numLatticePointsLusztig('D', 3, [0,1,1]; variant=2)
15
```
natural representation of ``\\mathfrak{o}_8``
```jldoctest
julia> PolyBases.Polytopes.numLatticePointsLusztig('D', 4, [1,0,0,0])
8

julia> PolyBases.Polytopes.numLatticePointsLusztig('D', 4, [1,0,0,0]; variant=2)
8
```
"""
function numLatticePointsLusztig(dynkin::Char, n::Int64, lambda::Vector{Int64}; variant=1::Int64) :: Int64
    @assert (n > 0)
    @assert (n == length(lambda))

    points = latticePointsLusztig(dynkin, n, lambda; variant=variant)
       
    return size(points,1)
end


end # module
