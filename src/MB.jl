module MB

export basisLieHighestWeight, setSerial, setParallel

using Oscar
using SparseArrays

#### vector space bases

ZZ = Int
TVec = SparseVector{ZZ, Int}
Short = UInt8 # for exponents of monomials; max. 255

struct VSBasis
    A::Vector{TVec}
    pivot::Vector{Int}
end


nullSpace() = VSBasis([], [])


function normalize(v::TVec)
    dropzeros!(v)
    if isempty(v.nzind)
        return (0, 0)
    end

    pivot = v.nzind[1]

    return v .÷ gcd(v.nzval), pivot
end


reduceCol(a, b, i::Int) = a*b[i] - b*a[i]


function addAndReduce!(sp::VSBasis, v::TVec)
    A = sp.A
    pivot = sp.pivot
    v, newPivot = normalize(v)
    if newPivot == 0
        return 0
    end

    for j = 1:length(A)
        i = pivot[j]
        if i != newPivot
            continue
        end
        v = reduceCol(v, A[j], i)
        v, newPivot = normalize(v)
        if newPivot == 0
            return 0
        end
    end

    pos = findfirst(pivot .> newPivot)
    if (pos === nothing)
        pos = length(pivot) + 1
    end

    insert!(A, pos, v)
    insert!(pivot, pos, newPivot)

    return v
end


#### Lie algebras

G = Oscar.GAP.Globals
forGap = Oscar.GAP.julia_to_gap
fromGap = Oscar.GAP.gap_to_julia


function lieAlgebra(t::String, n::Int)
    L = G.SimpleLieAlgebra(forGap(t), n, G.Rationals)
    return L, G.ChevalleyBasis(L)
end


gapReshape(A) = sparse(hcat(A...))


function matricesForOperators(L, hw, ops)
    M = G.HighestWeightModule(L, forGap(hw))
    mats = G.List(ops, o -> G.MatrixOfAction(G.Basis(M), o))
    mats = gapReshape.(fromGap(mats))
    d = lcm(denominator.(union(mats...)))
    mats = (A->ZZ.(A*d)).(mats)
    return mats
end


function weightsForOperators(L, cartan, ops)
    cartan = fromGap(cartan, recursive=false)
    ops = fromGap(ops, recursive=false)
    asVec(v) = fromGap(G.ExtRepOfObj(v))
    if any(iszero.(asVec.(ops)))
        error("ops should be non-zero")
    end
    nzi(v) = findfirst(asVec(v) .!= 0)
    return [
        [asVec(h*v)[nzi(v)] / asVec(v)[nzi(v)] for h in cartan] for v in ops
    ]
end

#### tensor model

# TODO: make the first one a symmetric product, or reduce more generally

spid(n) = spdiagm(0 => [ZZ(1) for _ in 1:n])
sz(A) = size(A)[1]
tensorProduct(A, B) = kron(A, spid(sz(B))) + kron(spid(sz(A)), B)
tensorProducts(As, Bs) = (AB->tensorProduct(AB[1], AB[2])).(zip(As, Bs))
tensorPower(A, n) = (n == 1) ? A : tensorProduct(tensorPower(A, n-1), A)
tensorPowers(As, n) = (A->tensorPower(A, n)).(As)


function tensorMatricesForOperators(L, hw, ops)
    mats = []

    for i in 1:length(hw)
        if hw[i] <= 0
            continue
        end
        wi = Int.(1:length(hw) .== i) # i-th fundamental weight
        _mats = matricesForOperators(L, wi, ops)
        _mats = tensorPowers(_mats, hw[i])
        mats = mats == [] ? _mats : tensorProducts(mats, _mats)
        #display(mats)
    end

    return mats
end

#### monomidal basis


# TODO: Demazure modules


"""
    basisLieHighestWeight(t::String, n::Int, hw::Vector{Int}) :: Tuple{Vector{Vector{Short}},Vector{TVec}}

Compute a monomial basis for the heighest weight module with highest weight ``hw`` (in terms of the fundamental weights), for a simple Lie algebra of type ``t`` and rank ``n``.

Example
======
```jldoctest
julia> dim, monomials, vectors = PolyBases.MB.basisLieHighestWeight("A", 2, [1,0])
(3, Vector{UInt8}[[0x00, 0x00, 0x00], [0x01, 0x00, 0x00], [0x00, 0x00, 0x01]], SparseArrays.SparseVector{Int64, Int64}[  [1]  =  1,   [2]  =  1,   [3]  =  1])
```
"""
function basisLieHighestWeight(t::String, n::Int, hw::Vector{Int}) :: Tuple{Int64,Vector{Vector{Short}},Vector{TVec}}
    # TODO: flag for root ordering
    L, CH = lieAlgebra(t, n)
    ops = CH[1] # positive root vectors
    # .. reorder..
    mats = tensorMatricesForOperators(L, hw, ops)
    wts = weightsForOperators(L, CH[3], ops)
    wts = (v->Int.(v)).(wts)

    #  display(wts)
    #  rnd = rand(length(wts[1]))
    #  rnd /= norm(rnd)
    #  wts = (v->round(dot(rnd,v), sigdigits=4)).(wts)
    #  display(wts)

    d = sz(mats[1])
    #display(mats)
    #display(d)
    hwv = spzeros(ZZ, d); hwv[1] = 1
    # TODO: (okay for now)
    # here we assume the first vector in G.Basis(M) is a highest weight vector

    #display(hwv)
    #display.(1. .* (mats))

    res = compute(hwv, mats, wts)

    return length(res[1]), res...
end

nullMon(m) = zeros(Short, m)


######### compute_0 ###################
function compute_0(v0, mats, wts::Vector{Vector{Int}})
    m = length(mats)
    monomials = [nullMon(m)]
    lastPos = 0

    id(mon) = sum((1 << (sum(mon[1:i])+i-1) for i in 1:m-1) , init = 1) # init = 1 for case m = 1
    e = [Short.(1:m .== i) for i in 1:m]
    maxid(deg) = id(deg.*e[1])

    blacklists = [falses(maxid(0))]
    blacklists[end][id(monomials[1])] = 1
    newMons(deg) = (push!(blacklists, falses(maxid(deg))))
    checkMon(mon) = blacklists[end-1][id(mon)]
    setMon(mon) = (blacklists[end][id(mon)] = true)

    vectors = [v0]
    weights = [0 * wts[1]]
    space = Dict(weights[1] => nullSpace())
    addAndReduce!(space[weights[1]], v0)

    deg = 0
    while length(monomials) > lastPos
        startPos = lastPos + 1
        newPos = length(monomials)
        deg = deg + 1
        newMons(deg)

        for i in 1:m, di in deg:-1:1
            for p in startPos:newPos
                # lex iterator
                if !all(monomials[p][1:i-1] .== 0) ||
                        monomials[p][i]+1 > di
                        startPos = p+1
                    continue
                elseif monomials[p][i]+1 < di
                    break
                end

                mon = monomials[p] + e[i]

                if any(i != j && mon[j] > 0 && !checkMon(mon-e[j]) for j in 1:m)
                    continue
                end

                wt = weights[p] + wts[i]
                if !haskey(space, wt)
                    space[wt] = nullSpace()
                end

                vec = mats[i] * vectors[p]
                vec = addAndReduce!(space[wt], vec)
                if vec == 0
                    continue
                end

                #display(v)
                #display(1. .* space.A)
                setMon(mon)
                push!(monomials, mon)
                push!(weights, wt)
                push!(vectors, vec)
            end
        end
        lastPos = newPos
    end

    return monomials, vectors
end


############ compute_p ########################
function compute_p(v0, mats, wts::Vector{Vector{Int}})
    m = length(mats)
    monomials = [nullMon(m)]
    lastPos = 0

    id(mon) = sum((1 << (sum(mon[1:i])+i-1) for i in 1:m-1), init = 1) # init = 1 for case m = 1
    e = [Short.(1:m .== i) for i in 1:m]
    maxid(deg) = id(deg.*e[1])

    blacklists = [falses(maxid(0))]
    blacklists[end][id(monomials[1])] = 1
    newMons(deg) = (push!(blacklists, falses(maxid(deg))))
    checkMon(mon) = blacklists[end-1][id(mon)]
    setMon(mon) = (blacklists[end][id(mon)] = true)

    vectors = [v0]
    weights = [0 * wts[1]]
    idweight = Dict(weights[1] => 1)
    nweights = 1
    space = Dict(1 => nullSpace())
    addAndReduce!(space[1], v0)

    deg = 0
    while length(monomials) > lastPos
        startPos = lastPos+1
        newPos = length(monomials)
        deg = deg + 1
        newMons(deg)

        num = 0
        jobs = Dict{Int, Vector{Tuple{Int, Vector{Short}, Vector{Int64}, SparseMatrixCSC{Int, Int}, SparseVector{Int, Int}, VSBasis}}}()

        for i in 1:m, di in deg:-1:1
            for p in startPos:newPos
                # lex iterator
                if !all(monomials[p][1:i-1] .== 0) ||
                        monomials[p][i] + 1 > di
                        startPos = p + 1
                    continue
                elseif monomials[p][i] + 1 < di
                    break
                end

                mon = monomials[p] + e[i]

                if any(i != j && mon[j] > 0 && !checkMon(mon - e[j]) for j in 1:m)
                    continue
                end

                num += 1
                wt = weights[p] + wts[i]

                if !haskey(idweight, wt)
                    nweights += 1
                    idweight[wt] = nweights
                end
                idwt = idweight[wt]

                if !haskey(space, idwt)
                    space[idwt] = nullSpace()
                end

                jo = (num, mon, wt, mats[i], vectors[p], space[idwt])
                if !haskey(jobs, idwt)
                    jobs[idwt] = [jo]
                else
                    push!(jobs[idwt], jo)
                end
                #display(typeof(jo))
            end
        end
        #display(typeof(jobs))

        #display(Threads.nthreads())

        res = Vector{Any}(nothing, num)
        idwts = [k for (k, l) in jobs if !isempty(l)]
        Threads.@threads for idwt in idwts
            for (i, mon, wt, A, v, sp) in jobs[idwt]
                # this is the heavy lifting
                # (in particular, the matrix product A * v)
                w = addAndReduce!(sp, A * v)
                if w != 0
                    res[i] = (mon, wt, w)
                end
            end
        end

        #display(res)

        for r in res
            if r == nothing
                continue
            end
            (mon, wt, vec) = r
            setMon(mon)
            push!(monomials, mon)
            push!(vectors, vec)
            push!(weights, wt)
        end

        lastPos = newPos
    end

    #display(idweight)
    #display(vectors)
    #display([(i, sp.A) for (i, sp) in space if length(sp.pivot) > 0])

    return monomials, vectors
end


##################
compute = compute_0


"""
setSerial()

Use serial mode.
"""
function setSerial() :: Nothing
    global compute = compute_0
    return nothing
end


"""
setParallel()

Use parallel mode.
"""
function setParallel() :: Nothing
    global compute = compute_p
    return nothing
end


end # module
