module WeylDim

export weylDim

"""
    weylDim(dynkin::Char, n::Int64, λ::Vector{Int64}) :: Int64

Returns the dimension of the hightest-weight module over the simple Lie algebra specified by `dynkin` and `n` with hightest weight ``ω = \\sum_{i=1}^{n} λ_i ω_i``. The algorithm uses Weyl's dimension formula.

Examples
======
Dynkin Type A
------
adjoint representation of ``\\mathfrak{sl}_4``
```jldoctest
julia> PolyBases.WeylDim.weylDim('A', 3, [1,0,1])
15
```
natural representation of ``\\mathfrak{sl}_5``
```jldoctest
julia> PolyBases.WeylDim.weylDim('A', 4, [1,0,0,0])
5
```

Dynkin Type B
------
adjoint representation of ``\\mathfrak{o}_7``
```jldoctest
julia> PolyBases.WeylDim.weylDim('B', 3, [0,1,0])
21
```
natural representation of ``\\mathfrak{o}_9``
```jldoctest
julia> PolyBases.WeylDim.weylDim('B', 4, [1,0,0,0])
9
```

Dynkin Type C
------
adjoint representation of ``\\mathfrak{sp}_6``
```jldoctest
julia> PolyBases.WeylDim.weylDim('C', 3, [2,0,0])
21
```
natural representation of ``\\mathfrak{sp}_8``
```jldoctest
julia> PolyBases.WeylDim.weylDim('C', 4, [1,0,0,0])
8
```

Dynkin Type D
------
adjoint representation of ``\\mathfrak{o}_6``
```jldoctest
julia> PolyBases.WeylDim.weylDim('D', 3, [0,1,1])
15
```
natural representation of ``\\mathfrak{o}_8``
```jldoctest
julia> PolyBases.WeylDim.weylDim('D', 4, [1,0,0,0])
8
```
"""
function weylDim(dynkin::Char, n::Int64, lambda::Vector{Int64}) :: Int64
    @assert (n > 0)
    @assert (n == length(lambda))

    if dynkin == 'A'
        res = weylDimA(n, lambda)
    elseif dynkin == 'B'
        res = weylDimB(n, lambda)
    elseif dynkin == 'C'
        res = weylDimC(n, lambda)
    elseif dynkin == 'D'
        res = weylDimD(n, lambda)
    else
        error("Dynkin type not supported")
    end

    return res
end


function weylDimA(n::Int64, lambda::Vector{Int64}) :: Int64
    @assert (n > 0)
    @assert (n == length(lambda))

    ll  = [(sum(lambda[i:n]) for i in 1:n)..., 0]
    rho = [n:-1:0...]

    return prod(1 + (ll[i] - ll[j]) // (rho[i] - rho[j]) for i in 1:n for j in i+1:n+1)
end


function weylDimB(n::Int64, lambda::Vector{Int64}) :: Int64
    @assert (n > 0)
    @assert (n == length(lambda))

    ll  = [(sum(lambda[i:n-1]) for i in 1:n-1)..., 0] .+ lambda[n]//2
    rho = [n-1:-1:0...] .+ 1//2

    return (
        prod(
            prod(
                (1 + (ll[i] - ll[j]) // (rho[i] - rho[j]))
                * (1 + (ll[i] + ll[j]) // (rho[i] + rho[j]))
                for j in i+1:n)
            * (1 + (ll[i] // rho[i]))
            for i in 1:n-1)
        * (1 + (ll[n] // rho[n])))
end


function weylDimC(n::Int64, lambda::Vector{Int64}) :: Int64
    @assert (n > 0)
    @assert (n == length(lambda))

    ll  = [(sum(lambda[i:n]) for i in 1:n)...]
    rho = [n-1:-1:0...] .+ 1

    return (
        prod(
            prod(
                (1 + (ll[i] - ll[j]) // (rho[i] - rho[j]))
                * (1 + (ll[i] + ll[j]) // (rho[i] + rho[j]))
                for j in i+1:n)
            * (1 + (ll[i] // rho[i]))
            for i in 1:n-1)
        * (1 + (ll[n] // rho[n])))
end


function weylDimD(n::Int64, lambda::Vector{Int64}) :: Int64
    @assert (n > 0)
    @assert (n == length(lambda))

    ll  = [(sum(lambda[i:n-2]) for i in 1:n-1)..., -lambda[n-1]] .+ (lambda[n-1] + lambda[n])//2
    rho = [n-1:-1:0...]

    return prod(
          (1 + (ll[i] - ll[j]) // (rho[i] - rho[j]))
        * (1 + (ll[i] + ll[j]) // (rho[i] + rho[j]))
        for i in 1:n-1 for j in i+1:n)
end

end # module
