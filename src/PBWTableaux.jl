module PBWTableaux

filterCol(m::Int, col::Vector{Int}, cond4::Bool = false) = (
    # condition (2)
    all(col[i] == i || col[i] > col[j] for i in 1:m for j in i+1:m) &&

    # condition (1)
    all(col[i] == i || col[i] > m for i in 1:m) &&

    # condition (4) for type C
    ( !cond4
    || all(col[i] != i || col[i] != 2n+1-col[j] for i in 1:m for j in i+1:m) )
)


# condition (3) for type C checks across cols
filterPbwC(t::Vector{Vector{Int}}) = all(t[j][i] <= maximum(t[j-1][i:end])
    for j in 2:length(t) for i in 1:length(t[j])
)


"""
    allTableaux(t::String, n::Int, mu::Vector{Int})

Returns a list of all PBW tableaux for type ``t``, rank ``n``, and shape ``mu``.
Currently works in types \"A\" and \"C\".
"""
function allTableaux(t::String, n::Int, mu::Vector{Int})
    if !(t in ["A", "C"])
        error("Supported types are A or C.")
    end
    typeC = (t == "C")
    N = (t == "C") ? 2n : n

    res = [[]]
    cols = []
    for (j, muj) in enumerate(mu)
        if j == 1 || muj != mu[j-1] # use already computed cols, if possible
            cols = [[c...] for c in Iterators.product((1:N for _ in 1:muj)...)
                    if isempty(c) || filterCol(muj, [c...], typeC)]
        end

        res = [ [t..., c] for t in res for c in cols ]

        if typeC
            filter!(filterPbwC, res)
        end
    end
    return res
end

 
"""
    printTableaux(result::Array)

Prints a list of PBW tableaux.
"""
function printTableaux(result::Array)
    res = ""
    for t in result
        for i in 1:length(t[1])
            res *= "  ".join((i < length(col) ? str(col[i]) : "") for col in t)
        end
    end
    println(res)
end


mu = [3, 3, 3]
n = 4
result = allTableaux("C", n, mu)
if length(result) < 15
    printTableaux(result)
end
display(length(result))

################### tests ###########

using Oscar
G = Oscar.GAP.Globals
forGap = Oscar.GAP.julia_to_gap


function testModule(t, n, mu)
    dim = length(allTableaux(t, n, mu))
    L = G.SimpleLieAlgebra(forGap(t), n, G.Rationals)
    dim_ref = G.Dimension(G.HighestWeightModule(L, forGap(mu)))
    @assert dim == dim_ref
end


function test()
    println("testing...")
    testModule("A", 2, [1, 1])
    testModule("A", 2, [2, 1])
    testModule("C", 2, [1, 0])
    testModule("C", 2, [0, 1])
    println("done.")
end

end # module
