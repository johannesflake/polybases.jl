module DyckPathA

using Combinatorics
using Oscar

VTuple{T} = Tuple{Vararg{T}}
Path = Vector{Tuple{Int64, Int64}}
Permutation = VTuple{Int64}

"""
Returns a list of inversions of a permutation.

```jldoctest
julia> PolyBases.DyckPathA.inversions((5,2,6,4,3,1))
11-element Vector{Tuple{Int64, Int64}}:
 (1, 1)
 (1, 3)
 (1, 4)
 (1, 5)
 (2, 5)
 (3, 3)
 (3, 4)
 (3, 5)
 (4, 4)
 (4, 5)
 (5, 5)
```

Prints the shape of a permutation.

The shape of a permutation w is an arrangement of inversions of w in a matrix with (i,j) in the i-th column and j-th row.

# Examples
```jldoctest
julia> PolyBases.DyckPathA.printShape((3,2,1))
O
OO
```


```jldoctest
julia> PolyBases.DyckPathA.printShape((5,2,6,4,3,1))
O
..
O.O
O.OO
OOOOO
```
"""
function shape(perm::Permutation) :: String
    n = length(perm) - 1
    res = ""
    for r in 1:n
        for c in 1:r
            res *= perm[c] > perm[r+1] ? "O" : "."
        end
        res *= "\n"
    end
    return res
end

printShape(perm::Permutation) = print(shape(perm))


################## uncommented ##########################

struct MaxDeg
    maxDegs :: Array{Dict}
end


"""
Set the rank of system.
"""
function initMaxDeg(n::Int64) :: MaxDeg
    ops = [(i, j) for i in 1:n for j in i:n]

    data = [Dict() for _ in 1:n]
    for i in 1:n
        iSets = Dict([
            Set(zip(cols, rows)) => length(rows)
            for rows in combinations(i:n)
            for colSet in combinations(1:i, length(rows))
            for cols in permutations(colSet)
        ])
        iSets[Set()] = 0
        data[i] = Dict([
            Set(subset) =>
            maximum(iSets[s] for s in keys(iSets) if s <= Set(subset))
            for subset in combinations(ops)
        ])
    end

    return MaxDeg(data)
end


"""
...
"""
function isSubPath(p::Path, q::Path) :: Bool
    return p[1][1] == q[1][1] &&
        p[end][2] == q[end][2] &&
        Set(p) < Set(q)
end


"""
Returns a list of all Dyck paths for a given permutation shape.
The set of inversions of ``w`` has a partial ``\\geq`` given by ``(i_1,j_1) \\geq (i_2,j_2)`` iff
``i_1 \\leq i_2`` and ``j_1 \\leq j_2``. A Dyck path is a sequence of inversions such that for
every pair of inversions ``(i_1,j_1) > (i_2,j_2)`` with ``i_2 \\leq j_1 +1, (i_1,j_2)`` is
also an inversion of ``w``. Further if ``i_2 \\leq j_1``, then ``(i_2,j_1)`` is also an inversion.

# Example
```jldoctest
julia> PolyBases.DyckPathA.dyck_paths((5,2,6,4,3,1))
18-element Vector{Vector{Tuple{Int64, Int64}}}:
 [(1, 1)]
 [(3, 3)]
 [(4, 4)]
 [(5, 5)]
 [(1, 1), (1, 3), (3, 3)]
 [(3, 3), (3, 4), (4, 4)]
 [(4, 4), (4, 5), (5, 5)]
 [(2, 5), (3, 5), (4, 5), (5, 5)]
 [(1, 1), (1, 3), (1, 4), (3, 4), (4, 4)]
 [(1, 1), (1, 3), (3, 3), (3, 4), (4, 4)]
 [(3, 3), (3, 4), (3, 5), (4, 5), (5, 5)]
 [(3, 3), (3, 4), (4, 4), (4, 5), (5, 5)]
 [(1, 1), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5)]
 [(1, 1), (1, 3), (1, 4), (1, 5), (3, 5), (4, 5), (5, 5)]
 [(1, 1), (1, 3), (1, 4), (3, 4), (3, 5), (4, 5), (5, 5)]
 [(1, 1), (1, 3), (1, 4), (3, 4), (4, 4), (4, 5), (5, 5)]
 [(1, 1), (1, 3), (3, 3), (3, 4), (3, 5), (4, 5), (5, 5)]
 [(1, 1), (1, 3), (3, 3), (3, 4), (4, 4), (4, 5), (5, 5)]
```
"""
function dyck_paths(perm::Permutation) :: Vector{Path}
    ops = inversions(perm)
    paths = [[o] for o in ops]
    newPaths = copy(paths)

    while !isempty(newPaths)
        p = popfirst!(newPaths)
        op = last(p)
        for o in ops
            if (o[1] == op[1] && o[2] > op[2]) ||
                    (o[2] == op[2] && o[1] > op[1])
                push!(paths, [p; o])
                push!(newPaths, [p; o])
            end
        end
    end

    paths = [
        p for p in paths
        if all((x[1], y[2]) in ops for (x, y) in combinations(p, 2))
        && all((y[1] > x[2]) || (y[1], x[2]) in ops for (x, y) in combinations(p, 2))
    ]
    return [p for p in paths if !any(isSubPath(p, q) for q in paths)]
end


"""
    dyck_poly(weight)

    dyck_poly(weight, permutation)

Computes a polytope based on Dyck paths.

Example
======
```jldoctest; filter = r"@0x[0-9a-z]+"
julia> PolyBases.DyckPathA.dyck_poly((1,0,0))
(Polymake.BigObjectAllocated(Ptr{Nothing} @0x000000000fdfa930), pm::Matrix<long>    
0 0 0
0 1 0
1 0 0
)
```
"""
function dyck_poly(m::VTuple{Int64})
    return dyck_poly(m, Tuple(length(m):-1:1))
end


function dyck_poly(m::VTuple{Int64}, perm::Permutation)
    n = length(m)
    ops = inversions(perm)
    if isempty(ops)
        return ((), (0,))
    end

    # positivity
    ieqs = [Int(a == b) for b in ops, a in [nothing; ops]]

    # dyck conditions
    for p in dyck_paths(perm)
        RHS = sum(m[p[1][1] : p[end][2]])
        LHS = [(a in p) for a in ops]
        ieqs = [ieqs; [RHS; -LHS]']
    end
    return mypolytope(ieqs)
end


"""
...
"""
function poly1(MD::MaxDeg, m::VTuple{Int64}, perm::Permutation)
    n = length(m)
    ops = inversions(perm)
    if isempty(ops)
        return ((), (0,))
    end

    # positivity
    ieqs = [Int(a == b) for b in ops, a in [nothing; ops]]

    # dyck conditions
    for p in dyck_paths(perm)
        RHS = sum(m[p[1][1] : p[end][2]])
        LHS = [(a in p) for a in ops]
        ieqs = [ieqs; [RHS; -LHS]']
    end

    # some total degrees
    for p in setdiff(dyck_paths(Tuple(n+1:-1:1)), dyck_paths(perm))
        if !(Set(p) <= Set(ops))
            continue
        end
        #if isempty(intersect(p, ops)) continue end
        RHS = sum(m .* [md[Set(p)] for md in MD.maxDegs])
        LHS = [(o in p) for o in ops]
        ieqs = [ieqs; [RHS; -LHS]']
    end

    (pot, pts) = mypolytope(ieqs)
    #println(size(pts, 1))
    #println(pts)

    ieqs = [0 for _ in 1:length(ops)+1]'

    # weight existence
    lamb = [[sum(m[i:end]) for i in 1:n]; 0]
    for ind in combinations(1:n+1)
        if length(ind) in (0, n+1) continue end
        RHS = sum(lamb[1:length(ind)]) - sum(lamb[ind])
        LHS = [-(a[1] in ind) + ((1+a[2]) in ind) for a in ops]
        ieqs = [ieqs; [RHS; -LHS]']
        #println([RHS; -LHS])
    end

    # all total degrees
    for subset in combinations(ops)
        RHS = sum(m .* [md[Set(subset)] for md in MD.maxDegs])
        LHS = [(o in subset) for o in ops]
        ieqs = [ieqs; [RHS; -LHS]']
    end
    #println(ieqs)

    # post-check
    ind = all(hcat([1 for _ in 1:size(pts, 1)], pts) * ieqs' .>= 0, dims=2)[:]
    #println(ind)
    pts = pts[ind, :]
    #println(size(pts, 1))

    return (pot, pts)
end


"""
...
"""
function mypolytope(ieqs :: Matrix{Int64})
    p = Oscar.Polymake.polytope.Polytope(INEQUALITIES=ieqs)
    pts = p.BOUNDARY_LATTICE_POINTS
    if length(p.INTERIOR_LATTICE_POINTS) > 0
        pts = vcat(pts, p.INTERIOR_LATTICE_POINTS)
    end
    return (p, sortslices(Int.(pts[:, 2:end]), dims=1))
end

"""
...
"""
function dimensions_all_w(m::VTuple{Int64}) :: Nothing
    n = length(m)
    lambda = [[sum(m[i:end]) for i in 1:n]; 0]

    sage_code = """
    RS = WeylCharacterRing(['A',$n], style=\"coroots\")
    print('Dict(')
    for w in WeylGroup(['A',$n]):
    print('{} => {},'.format(w.to_permutation(), sum(RS.demazure_character($lambda,word=w.reduced_word()).coefficients())))
    print(')')
    """
    sage_data = callSage(sage_code)
end


"""
...
"""
function test_poly1(m::VTuple{Int64}) :: Vector{VTuple{Int64}}
    n = length(m)
    sage_data = dimensions_all_w(m)
    MD = initMaxDeg(n)
    badOnes = []

    for perm in permutations(1:n+1)
        if isTriangular(perm)
            continue
        end

        perm = Tuple(perm)
        res = poly1(MD, m, perm)
        npts0 = sage_data[perm]
        npts = size(res[2], 1)

        if npts != npts0
            println("$npts0\t$npts\t$perm")
            push!(badOnes, perm)
        end
    end

    return badOnes
end


"""
...
"""
function test_dyck(m::VTuple{Int64})
    m = Tuple(m)
    n = length(m)
    sage_data = dimensions_all_w(m)
    MD = initMaxDeg(n)

    for perm in permutations(1:n+1)
        perm = Tuple(perm)
        res = dyck_poly(m, perm)
        npts0 = sage_data[perm]
        npts = size(res[2], 1)
        if npts != npts0
            println("$npts0\t$npts\t$perm")
        end
    end
end


"""
...
"""
function vshape(perm::Permutation, v) :: Nothing
    perm = Tuple(perm)
    n = length(perm) - 1
    for r in 1:n
        for c in 1:r
            print(perm[c] > perm[r+1] ? popfirst!(v) : ".")
        end
        println("")
    end
end


"""
...
"""
function isTriangular(perm::Permutation) :: Bool
    return all(p[1] <= p[3]
        || p[2] <= p[4]
        || (p[1] > p[4] && p[2] > p[3])
        for p in combinations(perm, 4))
end

#-------------

"""
...
"""
callSage(cmd) = eval(Meta.parse(join(readlines(`sage -c "$cmd"`), "")))


"""
...
"""
function sage_demazureCharacter(m::VTuple{Int64}, perm::Permutation)
    n = length(m)
    lambda = [[sum(m[i:end]) for i in 1:n]; 0]

    sage_code = """
    RS = WeylCharacterRing(['A',$n], style=\"coroots\")
    WG = WeylGroup(['A',$n])
    w = [w for w in WG if w.to_permutation()==tuple($perm)][0]
    ch = RS.demazure_character($lambda,word=w.reduced_word()).weyl_group_action(w.inverse())

    print('Dict(')
    for s in ch.support():
    print('{} => {},'.format(tuple(s.to_weight_space().to_vector()), ch.coefficient(s)))
    print(')')
    """
    return callSage(sage_code)
end


"""
function characterP(m,w,P)
    wts = [a.to_weight_space().to_vector() for a in inversions(w)]
    pts = P.integral_points()
    res = [hw(m).to_weight_space().to_vector()-sum(( c*wt for (c,wt) in zip(pt,wts) )) for pt in pts]
    return sum(( WeightRing(RS)(*x) for x in res ))
end
"""


"""
...
"""
function diffP(MD, m::VTuple{Int64}, perm::Permutation)
    ch0 = sage_demazureCharacter(m, perm)
    pts = poly1(MD, m, perm)[2]
    ch1 = weightsPts(m, perm, pts)

    extraWts = setdiff(keys(ch1), keys(ch0))
    if length(extraWts) > 0
        println("pts with non-existing weights:")
        println(union(Set(ch1[w]) for w in extraWts))
    end

    bDiff = false
    for w in keys(ch0)
        n0 = ch0[w]
        n1 = w in keys(ch1) ? size(ch1[w], 2) : 0
        if n0 == n1
            continue
        end
        bDiff = true
        println("diff: ", n1 - n0)
        if n1 == 0
            continue
        end
        for pt in ch1[w]
            vshape(perm, pt)
        end
    end

    if !bDiff
        print(0)
    end
end


###############################################
#
"""
find inversions
"""
function inversions(perm::Permutation) :: Vector{Tuple{Int64, Int64}}
    n = length(perm) - 1
    return [(i, j) for i in 1:n for j in i:n if perm[i] > perm[j+1]]
end


"""
 weight in lambda basis of operator (col,row)
"""
weightOp(n::Int64, col::Int64, row::Int64) = -[
    -Int(i == col-1) +
    Int(i == col) +
    Int(i == row) +
    -Int(i == row+1)
    for i in 1:n
]


"""
 weights in lambda basis for pts (1 pt / row)
"""
function weightsPts(m::VTuple{Int64}, perm::Permutation, pts) :: Dict
    m = [m...]
    n = length(perm) - 1
    weights = [weightOp(n, c, r) for (c, r) in inversions(perm)]
    allwts = map(x->x+m, pts * weights)
    uwts = unique(allwts)
    rowList(mat) = mapslices(x->[x], mat, dims=2)[:]
    return Dict(Tuple(uwt) => rowList(pts[findall(x->x==uwt, allwts), :]) for uwt in uwts)
end


end # module
