include("setup.jl")

@testset ExtendedTestSet "All PolyBases tests" begin
    include("DyckPathA-test.jl")
    include("MB-test.jl")
    include("PBWTableaux-test.jl")
    include("Polytopes-test.jl")
    include("WeylDim-test.jl")
end
