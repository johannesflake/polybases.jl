using Oscar
using Test
using TestSetExtensions

G = Oscar.GAP.Globals
forGap = Oscar.GAP.julia_to_gap
fromGap = Oscar.GAP.gap_to_julia

MB = PolyBases.MB

function testLieHighestWeight(dynkin::Char, n::Int64, lambda::Vector{Int64})
    L = G.SimpleLieAlgebra(forGap(string(dynkin)), n, G.Rationals)
    gapDim = G.DimensionOfHighestWeightModule(L, forGap(lambda))
    dim, m, v = MB.basisLieHighestWeight(string(dynkin), n, lambda) 

    @test gapDim == dim == length(m) == length(v)
end

@testset ExtendedTestSet "All MB tests" begin
    # TODO: add test for basis (not just dimension)

    @testset "MB serial tests" begin
        MB.setSerial()

        @testset "Dynking type $dynkin" for dynkin in ('A', 'B', 'C', 'D')
            @testset "n = $n" for n in 1:5  # see issue #6
                if (!(dynkin == 'B' && n < 2) && !(dynkin == 'C' && n < 2) && !(dynkin == 'D' && n < 4))
                    for i in 1:n                                # w_i
                        lambda = zeros(Int64,n)
                        lambda[i] = 1
                        testLieHighestWeight(dynkin, n, lambda)
                    end
                    
                    if (n > 1)
                        lambda = [1, (0 for i in 1:n-2)..., 1]  # w_1 + w_n
                        testLieHighestWeight(dynkin, n, lambda)
                    end

                    if (n < 4)
                        lambda = ones(Int64,n)                  # w_1 + ... + w_n
                        testLieHighestWeight(dynkin, n, lambda)
                    end
                end
            end
        end
    end

    @testset "MB parallel tests" begin
        MB.setParallel()
        @testset "Dynking type $dynkin" for dynkin in ('A', 'B', 'C', 'D')
            @testset "n = $n" for n in 1:5  # see issue #6
                if (!(dynkin == 'B' && n < 2) && !(dynkin == 'C' && n < 2) && !(dynkin == 'D' && n < 4))
                    for i in 1:n                                # w_i
                        lambda = zeros(Int64,n)
                        lambda[i] = 1
                        testLieHighestWeight(dynkin, n, lambda)
                    end
                    
                    if (n > 1)
                        lambda = [1, (0 for i in 1:n-2)..., 1]  # w_1 + w_n
                        testLieHighestWeight(dynkin, n, lambda)
                    end

                    if (n < 4)
                        lambda = ones(Int64,n)                  # w_1 + ... + w_n
                        testLieHighestWeight(dynkin, n, lambda)
                    end
                end
            end
        end
    end

    @testset "compute_0/p-Test single weight" begin
        res = PolyBases.MB.compute_p(sparse([1, 0]), sparse.([[0 0; 1 0]]), [[1]])
        res2 = PolyBases.MB.compute_0(sparse([1, 0]), sparse.([[0 0; 1 0]]), [[1]])
        @test res == res2
        @test res[1] == Vector{UInt8}[[0x00], [0x01]]
        @test res[2] == sparse.([[1, 0], [0, 1]])
    end
    
end
