using Test
using TestSetExtensions

using LinearAlgebra
using Oscar

using PolyBases

G = Oscar.GAP.Globals
forGap = Oscar.GAP.julia_to_gap
fromGap = Oscar.GAP.gap_to_julia
