using Test
using TestSetExtensions

@testset ExtendedTestSet "All DyckPathA tests" begin
    @testset "shape-Test" begin
        @test PolyBases.DyckPathA.shape((5,2,6,4,3,1)) == "O\n..\nO.O\nO.OO\nOOOOO\n"
        @test PolyBases.DyckPathA.shape((3,2,1)) == "O\nOO\n"
        @test PolyBases.DyckPathA.shape((1, 2, 3, 4, 5)) == ".\n..\n...\n....\n"
        @test PolyBases.DyckPathA.shape((4, 3, 2, 1)) == "O\nOO\nOOO\n"
        @test PolyBases.DyckPathA.shape((1, 4, 2, 8, 5, 7)) == ".\n.O\n...\n...O\n...O.\n"
    end

    @testset "dyck_paths-Test" begin
        @test PolyBases.DyckPathA.dyck_paths((5,2,6,4,3,1)) == [ [(1, 1)],
            [(3, 3)],
            [(4, 4)],
            [(5, 5)],
            [(1, 1), (1, 3), (3, 3)],
            [(3, 3), (3, 4), (4, 4)],
            [(4, 4), (4, 5), (5, 5)],
            [(2, 5), (3, 5), (4, 5), (5, 5)],
            [(1, 1), (1, 3), (1, 4), (3, 4), (4, 4)],
            [(1, 1), (1, 3), (3, 3), (3, 4), (4, 4)],
            [(3, 3), (3, 4), (3, 5), (4, 5), (5, 5)],
            [(3, 3), (3, 4), (4, 4), (4, 5), (5, 5)],
            [(1, 1), (1, 5), (2, 5), (3, 5), (4, 5), (5, 5)],
            [(1, 1), (1, 3), (1, 4), (1, 5), (3, 5), (4, 5), (5, 5)],
            [(1, 1), (1, 3), (1, 4), (3, 4), (3, 5), (4, 5), (5, 5)],
            [(1, 1), (1, 3), (1, 4), (3, 4), (4, 4), (4, 5), (5, 5)],
            [(1, 1), (1, 3), (3, 3), (3, 4), (3, 5), (4, 5), (5, 5)],
            [(1, 1), (1, 3), (3, 3), (3, 4), (4, 4), (4, 5), (5, 5)]]
        @test PolyBases.DyckPathA.dyck_paths((1, 2, 3, 4)) == []
        @test PolyBases.DyckPathA.dyck_paths((3, 2, 1)) == [[(1, 1)], 
        [(2,2)],
        [(1, 1), (1, 2), (2, 2)]]
    end

    @testset "inversions-Test" begin
        @test PolyBases.DyckPathA.inversions((5, 2, 6, 4, 3, 1)) == [(1, 1), (1, 3), (1, 4), (1, 5), (2, 5), (3, 3), (3, 4), (3, 5), (4, 4), (4, 5), (5, 5)]
        @test PolyBases.DyckPathA.inversions((1, 2, 3, 4, 5)) == []
        @test PolyBases.DyckPathA.inversions((5, 4, 3, 2, 1)) == [(1, 1), (1, 2), (1, 3), (1, 4), (2, 2), (2, 3), (2, 4), (3, 3), (3, 4), (4, 4)]
        @test PolyBases.DyckPathA.inversions((1, 3, 2, 6, 4, 5)) == [(2, 2), (4, 4), (4, 5)]
        @test PolyBases.DyckPathA.inversions((6, 1, 5, 2, 4, 3)) == [(1, 1), (1, 2), (1, 3), (1, 4), (1, 5), (3, 3), (3, 4), (3, 5), (5, 5)]
    end

    @testset "dyck_poly-Test" begin
        @test PolyBases.DyckPathA.dyck_poly(()) == ((), (0,))
        res1, res2 = PolyBases.DyckPathA.dyck_poly((1,0,0))
        @test res2 == [0 0 0; 0 1 0; 1 0 0]
    end

    @testset "isSubPath-Test" begin
        #path1 < path[2] < path[3] != path[3]
        path1 = [(1, 9)]
        path2 = [(1, 2), (3, 4), (1, 9)]
        path3 = [(1, 2), (3, 4), (8, 9), (1, 9)]
        path4 = [(1, 1), (1, 3), (1, 4), (3, 4), (4, 4), (4, 5), (5, 5)]
        @test PolyBases.DyckPathA.isSubPath(path1, path1) == false
        @test PolyBases.DyckPathA.isSubPath(path1, path2) == true
        @test PolyBases.DyckPathA.isSubPath(path1, path3) == true
        @test PolyBases.DyckPathA.isSubPath(path2, path3) == true
        @test PolyBases.DyckPathA.isSubPath(path2, path4) == false
    end
end
