WD = PolyBases.WeylDim

@testset ExtendedTestSet "All WeylDim tests" begin
    @testset "Input sanitizer tests" begin
        @testset "Dynking type $dynkin" for dynkin in ('A', 'B', 'C', 'D')
            @test_throws AssertionError WD.weylDim(dynkin, 0, Int64[])

            @test_throws AssertionError WD.weylDim(dynkin, 3, [2,5])
            @test_throws AssertionError WD.weylDim(dynkin, 2, [1,4,7,8])
            @test_throws AssertionError WD.weylDim(dynkin, 6, Int64[])
        end
    end

    @testset "weylDim tests" begin
        @testset "Dynking type $dynkin" for dynkin in ('A', 'B', 'C', 'D')
            @testset "n = $n" for n in 1:5
                if (!(dynkin == 'B' && n < 2) && !(dynkin == 'C' && n < 2) && !(dynkin == 'D' && n < 4))
                    L = G.SimpleLieAlgebra(forGap(string(dynkin)), n, G.Rationals)

                    for i in 1:n                                # w_i
                        lambda = zeros(Int64,n)
                        lambda[i] = 1
                        @test WD.weylDim(dynkin, n, lambda) == G.DimensionOfHighestWeightModule(L, forGap(lambda))
                    end
                    
                    if (n > 1)
                        lambda = [1, (0 for i in 1:n-2)..., 1]  # w_1 + w_n
                        @test WD.weylDim(dynkin, n, lambda) == G.DimensionOfHighestWeightModule(L, forGap(lambda))
                    end

                    if (n <= 4)
                        lambda = ones(Int64,n)                  # w_1 + ... + w_n
                        @test WD.weylDim(dynkin, n, lambda) == G.DimensionOfHighestWeightModule(L, forGap(lambda))
                    end
                end
            end
        end
    end
    
end
