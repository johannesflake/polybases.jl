PT = PolyBases.Polytopes

@testset ExtendedTestSet "All Polytopes tests" begin
    @testset "Input sanitizer tests" begin
        @testset "Dynking type $dynkin, variant $var" for dynkin in ('A', 'B', 'C', 'D'), var in (1,2)
            if !(dynkin == 'A' && var != 1)
                @test_throws AssertionError PT.numLatticePointsLusztig(dynkin, 0, Int64[], variant=var)
                @test_throws AssertionError PT.latticePointsLusztig(dynkin, 0, Int64[], variant=var)
                @test_throws AssertionError PT.ineqLusztig(dynkin, 0, Int64[], variant=var)

                @test_throws AssertionError PT.numLatticePointsLusztig(dynkin, 3, [2,5], variant=var)
                @test_throws AssertionError PT.latticePointsLusztig(dynkin, 2, [1,4,7,8], variant=var)
                @test_throws AssertionError PT.ineqLusztig(dynkin, 6, Int64[], variant=var)
            end
        end
    end

    @testset "latticePoints tests" begin
        @test PT.latticePointsLusztig('A', 2, [1,0]) == [1 0 0 0; 1 0 1 0; 1 1 0 0]
        @test PT.latticePointsLusztig('A', 2, [0,1]) == [1 0 0 0; 1 0 0 1; 1 1 0 1]
        @test PT.latticePointsLusztig('A', 2, [1,1]) == [1 0 0 0; 1 0 0 1; 1 0 1 0; 1 0 1 1; 1 1 0 0; 1 1 0 1; 1 1 1 1; 1 2 0 1]

        @test PT.latticePointsLusztig('B', 2, [1,0]) == [1 0 0 0 0; 1 0 0 1 0; 1 0 1 0 0; 1 1 0 0 0; 1 1 0 1 0]
        @test PT.latticePointsLusztig('B', 2, [0,1]) == [1 0 0 0 0; 1 0 0 0 1; 1 0 1 0 1; 1 1 0 0 1]

        @test PT.latticePointsLusztig('B', 2, [1,0], variant=2) == [1 0 0 0 0; 1 0 0 1 0; 1 0 1 0 0; 1 1 0 0 0; 1 1 0 1 0]
        @test PT.latticePointsLusztig('B', 2, [0,1], variant=2) == [1 0 0 0 0; 1 0 0 0 1; 1 0 1 0 1; 1 1 0 0 1]

        @test PT.latticePointsLusztig('C', 2, [1,0]) == [1 0 0 0 0; 1 0 0 1 0; 1 1 0 0 0; 1 1 0 1 0]
        @test PT.latticePointsLusztig('C', 2, [0,1]) == [1 0 0 0 0; 1 0 0 0 1; 1 0 1 0 1; 1 1 0 0 1; 1 2  0 0 1]

        @test PT.latticePointsLusztig('C', 2, [1,0], variant=2) == [1 0 0 0 0; 1 0 0 1 0; 1 1 0 0 0; 1 1 0 1 0]
        @test PT.latticePointsLusztig('C', 2, [0,1], variant=2) == [1 0 0 0 0; 1 0 0 0 1; 1 0 1 0 1; 1 1 0 0 1; 1 2  0 0 1]

        @test PT.latticePointsLusztig('D', 4, [1,0,0,0]) == [1 0 0 0 0 0 0 0 0 0 0 0 0;
                                                             1 0 0 0 0 0 0 0 0 0 1 0 0;
                                                             1 0 0 0 0 0 1 0 0 0 1 0 0;
                                                             1 0 0 0 1 0 0 0 0 0 0 0 0;
                                                             1 0 0 1 0 0 1 0 0 0 1 0 0;
                                                             1 0 1 0 0 0 0 0 0 0 0 0 0;
                                                             1 1 0 0 0 0 0 0 0 0 0 0 0;
                                                             1 1 0 1 0 0 1 0 0 0 1 0 0]

        @test PT.latticePointsLusztig('D', 4, [1,0,0,0], variant=2) == [1 0 0 0 0 0 0 0 0 0 0 0 0;
                                                                        1 0 0 0 0 0 0 0 0 0 1 0 0;
                                                                        1 0 0 0 0 0 1 0 0 0 0 0 0;
                                                                        1 0 0 0 1 0 0 0 0 0 1 0 0;
                                                                        1 0 0 1 0 0 1 0 0 0 0 0 0;
                                                                        1 0 1 0 0 0 0 0 0 0 1 0 0;
                                                                        1 1 0 0 0 0 0 0 0 0 1 0 0;
                                                                        1 1 0 1 0 0 1 0 0 0 0 0 0]
        
    end


    @testset "Dimension / numLatticePoints tests" begin
        @testset "Dynking type $dynkin, variant $var" for dynkin in ('A', 'B', 'C', 'D'), var in (1,2)
            if !(dynkin == 'A' && var != 1)
                @testset "n = $n" for n in 1:5
                    if (!(dynkin == 'B' && n < 2) && !(dynkin == 'C' && n < 2) && !(dynkin == 'D' && n < 4))
                        L = G.SimpleLieAlgebra(forGap(string(dynkin)), n, G.Rationals)

                        for i in 1:n                                # w_i
                            lambda = zeros(Int64,n)
                            lambda[i] = 1
                            @test PT.numLatticePointsLusztig(dynkin, n, lambda, variant=var) == G.DimensionOfHighestWeightModule(L, forGap(lambda))
                        end
                        
                        if (n > 1)
                            lambda = [1, (0 for i in 1:n-2)..., 1]  # w_1 + w_n
                            @test PT.numLatticePointsLusztig(dynkin, n, lambda, variant=var) == G.DimensionOfHighestWeightModule(L, forGap(lambda))
                        end

                        if (n < 4)
                            lambda = ones(Int64,n)                  # w_1 + ... + w_n
                            @test PT.numLatticePointsLusztig(dynkin, n, lambda, variant=var) == G.DimensionOfHighestWeightModule(L, forGap(lambda))
                        end
                    end
                end
            end
        end
    end
    
end
