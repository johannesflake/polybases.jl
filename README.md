# PolyBases.jl

[![Dev](https://img.shields.io/badge/docs-dev-blue.svg)](https://johannesflake.gitlab.io/polybases.jl/dev)
[![Build Status](https://gitlab.com/johannesflake/polybases.jl/badges/master/pipeline.svg)](https://gitlab.com/johannesflake/polybases.jl/pipelines)
[![Coverage](https://gitlab.com/johannesflake/polybases.jl/badges/master/coverage.svg)](https://gitlab.com/johannesflake/polybases.jl/commits/master)

A [Julia](https://julialang.org/) package for constructing combinatorial bases of highest weight representations of simple Lie algebras and their submodules.


## Install

To install this package in Julia:
```
using Pkg; Pkg.add("https://gitlab.com/johannesflake/polybases.jl.git")
```

If you have not already installed Julia, you can install by following step 1 and step 2 on the [OSCAR installation page](https://oscar.computeralgebra.de/install/).


## Basic usage

Preliminary documentation can be found at [https://johannesflake.gitlab.io/polybases.jl/](https://johannesflake.gitlab.io/polybases.jl/).


## General Disclaimer

All code in this repository is preliminary work.

It comes with absolutely no warranty and will most likely have errors. If you use it for computations, please check the correctness of the result very carefully.

Also, everything in this repository might change in the future, so currently any update can break the code you wrote upon functionality from packages in this repository.

This software is licensed under the GPL, version 3, or any later version.


## Funding

The development of this Julia package is supported by the Deutsche Forschungsgemeinschaft DFG within the [Collaborative Research Center TRR 195](https://www.computeralgebra.de/sfb/).
