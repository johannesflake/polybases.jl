using Profile

include("../src/MB.jl")

function benchmark()
    @time MB.basisLieHighestWeight("B", 3, [1,1,1]);
    @time MB.basisLieHighestWeight("A", 5, [1,1,1,1,0]);
    @time MB.basisLieHighestWeight("B", 4, [1,1,1,0]);
    @time MB.basisLieHighestWeight("B", 4, [0,1,1,1]);
    return
end

function benchmark2()
    @time MB.basisLieHighestWeight("B", 4, [1,1,1,1]);
    @time MB.basisLieHighestWeight("A", 5, [1,1,1,1,1]);
    return
end

function benchmark_operators()
    L, CH = lieAlgebra("B", 4)
    hw = [1,1,1,1]
    ops = CH[1] # positive root vectors
    #ops = G.CanonicalGenerators(G.RootSystem(L))[1]
    @time tensorMatricesForOperators(L, hw, ops)
end

function profile()
  Profile.clear()
  Profile.@profile MB.basisLieHighestWeight("B", 4, [1,1,1,1])
  Profile.print(maxdepth=20)
end
