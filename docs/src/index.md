# PolyBases.jl Documentation

## Docs
```@autodocs
Modules = [PolyBases, PolyBases.DyckPathA, PolyBases.MB, PolyBases.PBWTableaux, PolyBases.Polytopes, PolyBases.WeylDim]
```

## Index
```@index
Modules = [PolyBases, PolyBases.DyckPathA, PolyBases.MB, PolyBases.PBWTableaux, PolyBases.Polytopes, PolyBases.WeylDim]
```
