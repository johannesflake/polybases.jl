using Documenter
using PolyBases

DocMeta.setdocmeta!(
    PolyBases,
    :DocTestSetup,
    :(using PolyBases);
    recursive = true,
)

makedocs(
    modules=[PolyBases, PolyBases.DyckPathA, PolyBases.MB, PolyBases.PBWTableaux, PolyBases.Polytopes, PolyBases.WeylDim],
    repo = "https://gitlab.com/johannesflake/PolyBases.jl/blob/{commit}{path}#{line}",
    sitename = "PolyBases.jl",
    checkdocs = :none,
    strict = true,
    format = Documenter.HTML(;
        prettyurls = get(ENV, "CI", nothing) == "true",
        canonical = "https://johannesflake.gitlab.io/PolyBases.jl",
    ),
    pages = ["Home" => "index.md"],
)

deploydocs(
    repo = "gitlab.com/johannesflake/PolyBases.jl.git",
    deploy_config = Documenter.GitLab(),
    branch = "pages",
    dirname = "public",
    devbranch = "master",
)
